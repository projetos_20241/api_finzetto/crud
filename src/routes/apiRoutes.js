const router = require('express').Router()
const teacherController = require('../controller/teacherController')
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')

router.get('/', teacherController.getTeacher)
router.put('/cadastroTeacher/', teacherController.updateTeacher)
router.delete('/deleteTeacher/:id', teacherController.deleteTeacher)
router.post('/postTeacher/', teacherController.postTeacher)

router.get('/external/', JSONPlaceholderController.getUsers);
router.get('/external/io', JSONPlaceholderController.getUsersWebSiteIO)
router.get('/external/com', JSONPlaceholderController.getUsersWebSiteCOM)
router.get('/external/net', JSONPlaceholderController.getUsersWebSiteNET)

router.get('/external/filter', JSONPlaceholderController.getCountDomain)
module.exports = router