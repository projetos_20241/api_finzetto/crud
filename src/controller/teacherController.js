module.exports = class teacherController {
    // Listar Docentes
    static async getTeacher(req, res) {
        res.status(200).json({
            algumaMensagem: {
                algumaMensagem: 'TESTING',
                algumaNúmero: 26,
            }
        });
    }

    // Cadastrar Usuário
    static async postTeacher(req, res) {
        const { nome, idade, profissão, curso } = req.body;
        console.log('Nome: ' + nome)
        console.log('Idade' + idade)
        console.log('Profissão' + profissão)
        console.log('Curso' + curso)
        res.status(200).json({
            message: "O aluno cadastrado chama-se:" + nome
        })
    }

    // Update Teacher
    static async updateTeacher(req, res) {
        const { nome, idade, profissão, curso } = req.body;
        if (nome !== "" && idade !== null && profissão !== "" && curso !== "") {
            return res.status(200).json({ message: 'Aluno Editado.' });
        }
        else {
            return res.status(200).json({ message: 'Erro ao Editar.' });
        }
    }

    // Delete Teacher
    static async deleteTeacher(req, res) {
        const { id } = req.params.id;
        if (id !== "") {
            return res.status(200).json({ message: 'Aluno Deletado.' });
        } else {
            return res.status(400).json({ message: 'Erro ao Deletar.' });
        }

    }

};
