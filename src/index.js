const express = require('express')
const app = express()

class AppController {
    constructor() {
        this.express = express();
        this.middlewares();
        this.routes();
    }

    middlewares() {
        this.express.use(express.json());
    }

    routes() {
        const apiRoutes= require('./routes/apiRoutes')  
        this.express.use('/api',apiRoutes)

    }
}

module.exports = new AppController().express;